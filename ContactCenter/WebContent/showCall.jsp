<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Call details</title>
</head>
<body>

	Call details
	<br>
	<br>

	<table width="50%">
		<tr>
			<td>ID:</td>
			<td><s:text name="%{call.uuid}"></s:text></td>
		</tr>
		<tr>
			<td>Date:</td>
			<td><s:date name="%{call.date}" format="dd/MM/yyyy" ></s:date></td>
		</tr>
		<tr>
			<td>Title:</td>
			<td><s:text name="%{call.title}" ></s:text></td>
		</tr>
		<tr>
			<td>Details:</td>
			<td><s:text name="%{call.problem}" ></s:text></td>
		</tr>

	</table>





</body>
</html>