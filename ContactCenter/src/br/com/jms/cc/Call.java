package br.com.jms.cc;
import java.io.Serializable;
import java.util.Date;

public class Call implements Serializable {

	private static final long serialVersionUID = 1L;
    private Long uuid;
    private String title;
    private Date date;
    private String problem;
    
	public Long getUuid() {
		return uuid;
	}
	public void setUuid(Long uuid) {
		this.uuid = uuid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getProblem() {
		return problem;
	}
	public void setProblem(String problem) {
		this.problem = problem;
	}
    
    
    
    
    
    
    


}
