package br.com.jms.cc.service;

import br.com.jms.cc.Call;

public interface ContactCenterService {
	
	public Call getNextCall()  throws Exception ;
	public Long createCall(Call call) throws Exception ;

}
