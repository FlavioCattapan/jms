package br.com.jms.cc.service;

import java.io.Serializable;
import java.util.Date;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.naming.InitialContext;
import br.com.jms.cc.Call;

public class ContactCenterServiceImpl implements ContactCenterService {
	
//	No modelo ponto a ponto, ou por filas, um "produtor" (producer) 
//	envia mensagens para uma fila e um "consumidor" (consumer) as l�. 
//	Neste caso, o produtor conhece o destino da mensagem e a envia diretamente 
//	para a fila do consumidor. Este modelo � caracterizado pelo seguinte:
//	    apenas um consumidor ir� ler a mensagem;
//	    n�o � necess�rio que o produtor esteja em execu��o no momento em que o 
//	    consumidor l� a mensagem, assim como n�o � necess�rio que o consumidor 
//	    esteja em execu��o no momento que o produtor envia a mensagem;
//	    quando l� uma mensagem com sucesso o consumidor envia um aviso 
//	    (acknowledged) para o produtor.


	private static final int TIMEOUT = 30000;

	// consumer
	public Call getNextCall () throws Exception {
		ConnectionFactory connectionFactory = null;
		// � um objeto administrado que encapsula a identidade do destino das
		// mensagens,
		// que � onde as mensagens s�o enviadas e consumidas. Pode ser uma fila ou
		// um t�pico.
		// O administrador JMS cria estes objetos, e os usu�rios os obt�m atrav�s de
		// buscas na JNDI.
		// Da mesma forma que as factories de conex�es,
		// o administrador pode criar dois tipos de classe de destino, fila e
		// t�pico.
		Destination destination = null;
		Session session = null;
		Connection connection = null;

		try {
			connectionFactory = getConnectionFactory();
			destination = getDestination();
			connection = connectionFactory.createConnection();
			// sess�o n�o ser� transacional
			// n�o precisa chamar o metodo commit
			// O recebimento da mensagens ser� notificado automaticamente.
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			// inicia o recebimento das mensagem
		    connection.start();
		    // consumidor de mensagens
		    MessageConsumer consumer = session.createConsumer(destination);
		    // desiste em 30 segundos
		    Message message = consumer.receive(TIMEOUT);
		    // nulo caso timeout
		    if(message == null){
		    	return null;		    	
		    }
		    // objeto seja retornado
		    Serializable object = ((ObjectMessage)message).getObject();
			return (Call)object;

		} finally {
			try {
				// fecha a sess�o do jms
				session.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				// fecha a conex�o do jms
				connection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}
 
	// producer
	// metodo responsavel por criar um chamado para o help desk
	public Long createCall(Call call) throws Exception {
		ConnectionFactory connectionFactory = null;
		Destination destination = null;
		Session session = null;
		Connection connection = null;

		try {
			Long uuid = System.currentTimeMillis();
			call.setDate(new Date());
			call.setUuid(uuid);
			connectionFactory = getConnectionFactory();
			destination = getDestination();
			connection = connectionFactory.createConnection();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer producer = session.createProducer(destination);
			Message message = session.createObjectMessage(call);
			producer.send(message);
			return uuid;

		} finally {
			try {
				session.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				connection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	// aplica��o cliente utiliza para a cria��o de conex�es para o provedor JMS
	//  obter� uma factory para a fila 
	private ConnectionFactory getConnectionFactory() throws Exception {
		InitialContext ic = new InitialContext();
		return (ConnectionFactory) ic
				.lookup("java:comp/env/jms/ConnectionFactory");
	}

	// encapsula a identidade do destino das mensagens
	// que � onde as mensagens s�o enviadas e consumidas
	// estes objetos devem ser criados no servidor
	private Destination getDestination() throws Exception {
		InitialContext ic = new InitialContext();
		return (Destination) ic.lookup("java:comp/env/jms/TestQueue");
	}

}
