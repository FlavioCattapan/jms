package br.com.jms.cc;

import br.com.jms.cc.service.ContactCenterService;
import br.com.jms.cc.service.ContactCenterServiceImpl;

import com.opensymphony.xwork2.ActionSupport;

public class ContactCenterAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private Call call;
	private ContactCenterService service = new ContactCenterServiceImpl();
    
	public Call getCall() {
		return call;
	}

	public void setCall(Call call) {
		this.call = call;
	}
	
	
	public String createCall() throws Exception {
	Long uuid = service.createCall(call);
	call.setUuid(uuid);
		return SUCCESS;
	}
	
	
	public String getNextCall() throws Exception {
  	this.call  = service.getNextCall();
  	   if(call == null){
  		   return "noCall";
  	   
  	   }
  			   
		return SUCCESS;
	}
	 
	

}
