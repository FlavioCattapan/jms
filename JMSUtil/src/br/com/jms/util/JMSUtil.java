package br.com.jms.util;


import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.naming.Context;
import javax.naming.InitialContext;

// Facilita a obten��o da ConnectionFactory e da Destination 
public class JMSUtil {

	// um t�pico (Topic) � publicado e v�rios assinantes (Subscribes) podem consumir a mensagem 
	
	private static final String CONNECTION_FACTORY = "TopicConnectionFactory";
	private static final String TOPIC = "topic/testTopic";
	// permite que aplica��es cliente descubram e obtenham dados ou objetos atrav�s de um nome
	// � utilizada em aplica��es Java que acessam recursos externos
	// objetos administrados num servi�o de diret�rio disponibilizado pelo servidor de aplica��es 
	private static Context jndiContext = null;
	
	// InitialContext le o arquivo jndi.properties que especifica onde procurar os objetos
	// java.naming.factory.initial = classe ''factory'' do contexto 
	// java.naming.provider.url = URL definindo a localiza��o do objeto
	public static Object jndiLookup(String jndiName) throws Exception {
		if (jndiContext == null) {
			try {
				jndiContext = new InitialContext();
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		return  jndiContext.lookup(jndiName);
	}
	
	// aplica��o cliente utiliza para a cria��o de conex�es para o provedor JMS
	//  obter� uma factory para t�pico 
	public static ConnectionFactory getConnectionFactory() throws Exception {
	    return (ConnectionFactory)jndiLookup(CONNECTION_FACTORY);
	}
	
	// encapsula a identidade do destino das mensagens
	// que � onde as mensagens s�o enviadas e consumidas
	// estes objetos devem ser criados no servidor
	public static  Destination  getDestination() throws Exception {
		return (Destination)jndiLookup(TOPIC);
	}
	

}
