package br.com.jms.util;

import java.io.Serializable;

public class StockInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String stockSymbol;
	private double price;
	private float var;

	public StockInfo(String stockSymbol, double price, float var) {
		super();
		this.stockSymbol = stockSymbol;
		this.price = price;
		this.var = var;
	}

	public String getStockSymbol() {
		return stockSymbol;
	}

	public void setStockSymbol(String stockSymbol) {
		this.stockSymbol = stockSymbol;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public float getVar() {
		return var;
	}

	public void setVar(float var) {
		this.var = var;
	}

}
