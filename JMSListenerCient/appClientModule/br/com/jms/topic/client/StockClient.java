package br.com.jms.topic.client;

import java.util.Iterator;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import br.com.jms.util.JMSUtil;
import br.com.jms.util.StockInfo;

@SuppressWarnings("unchecked")
// JMS Message Listeners
// Objeto recebe as mensagens de modo ass�ncrono
public class StockClient extends JFrame implements MessageListener {

	private static final long serialVersionUID = 1L;
	private static final String[] HEADERS = { "Stock", "Price", "Variation" };
	private JTable table = new JTable(new DefaultTableModel(HEADERS, 0));
	private JScrollPane scrollPane = new JScrollPane(table);
	private ConnectionFactory cf;
	private Connection connection;
	private Session session;
	// � um objeto administrado que encapsula a identidade do destino das
	// mensagens,
	// que � onde as mensagens s�o enviadas e consumidas. Pode ser uma fila ou
	// um t�pico.
	// O administrador JMS cria estes objetos, e os usu�rios os obt�m atrav�s de
	// buscas na JNDI.
	// Da mesma forma que as factories de conex�es,
	// o administrador pode criar dois tipos de classe de destino, fila e
	// t�pico.
	private Destination destination;
	// Um objeto criado atrav�s de uma sess�o JMS. Ele recebe mensagens de um
	// destino.
	// O consumidor pode receber mensagens de maneira s�ncrona ou ass�ncrona de
	// filas ou t�picos.
	private MessageConsumer consumer;

	private void starttListener() throws Exception {
		try {
			cf = JMSUtil.getConnectionFactory();
			destination = JMSUtil.getDestination();
			connection = cf.createConnection();
			// O recebimento da mensagens ser� notificado automaticamente.
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			consumer = session.createConsumer(destination);
			consumer.setMessageListener(this);
			// come�a a receber as mensagens
			connection.start();
		} catch (Exception e) {
			try {
				e.printStackTrace();
				connection.close();
			} catch (Exception e2) {

			}
		}

	}
    
	// configura a tela de swing
	public StockClient() throws Exception {
		setLocation(100, 100);
		setSize(350, 100);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().add(scrollPane);
		setVisible(true);
		starttListener();
	}

	public static void main(String[] args) throws Exception {
		new StockClient();
	}

	// a��es que s�o chamadas quando a mensagem chega
	public void onMessage(Message message) {
		System.out.println(message);
		try {
			List<StockInfo> stockInfos = (List<StockInfo>) ((ObjectMessage) message)
					.getObject();
			String[][] data = new String[stockInfos.size()][3];
			int i = 0;
			for (Iterator iterator = stockInfos.iterator(); iterator.hasNext(); i++) {
				StockInfo stockInfo = (StockInfo) iterator.next();
				data[i][0] = stockInfo.getStockSymbol();
				data[i][1] = String.valueOf(stockInfo.getPrice());
				data[i][2] = String.valueOf(stockInfo.getVar());
			}
			table.setModel(new DefaultTableModel(data, HEADERS));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
