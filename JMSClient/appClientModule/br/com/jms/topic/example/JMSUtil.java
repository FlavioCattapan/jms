package br.com.jms.topic.example;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.naming.Context;
import javax.naming.InitialContext;

public class JMSUtil {

	private static final String CONNECTION_FACTORY = "TopicConnectionFactory";
	private static final String TOPIC = "topic/testTopic";
	private static Context jndiContext = null;

	public static Object jndiLookup(String jndiName) throws Exception {
		if (jndiContext == null) {
			try {
				jndiContext = new InitialContext();
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		return  jndiContext.lookup(jndiName);
	}
	
	public static ConnectionFactory getConnectionFactory() throws Exception {
	    return (ConnectionFactory)jndiLookup(CONNECTION_FACTORY);
	}
	
	public static  Destination  getDestination() throws Exception {
		return (Destination)jndiLookup(TOPIC);
	}
	

}
