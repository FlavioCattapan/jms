package br.com.jms.topic.example;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

public class Listener implements MessageListener{
	
	private ConnectionFactory connectionFactory;
	private Connection connection;
	private Session session;
	private Destination destination;
	
	public void setup() throws Exception{
		try {
			connectionFactory = JMSUtil.getConnectionFactory();
			destination = JMSUtil.getDestination();
			connection = connectionFactory.createConnection();
			// O recebimento da mensagens ser� notificado automaticamente.
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		    MessageConsumer consumer = session.createConsumer(destination);
		   consumer.setMessageListener(this);
		   connection.start();
		   System.in.read();
		} finally  {
			try {
			connection.close();
			} catch (Exception e){
				
			}
			
		}
		connectionFactory = JMSUtil.getConnectionFactory();
	}
	
	
	public static void main(String[] args) throws Exception{
		new Listener().setup();
	}


	public void onMessage(Message message) {
		System.out.println(message);
		
	}
	

}
