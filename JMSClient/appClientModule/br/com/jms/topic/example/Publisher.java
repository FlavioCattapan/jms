package br.com.jms.topic.example;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

public class Publisher {
	
	private ConnectionFactory connectionFactory;
	private Connection connection;
	private Session session;
	private Destination destination;
	
	public void sendMessage() throws Exception{
		try {
			connectionFactory = JMSUtil.getConnectionFactory();
			destination = JMSUtil.getDestination();
			connection = connectionFactory.createConnection();
			// Session.AUTO_ACKNOWLEDGE n�o precisa dar o commit quando der o send ja envia
			// O recebimento da mensagens ser� notificado automaticamente.
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		    MessageProducer messageProducer = session.createProducer(destination);
		    TextMessage message = session.createTextMessage("Minha mensagem bonitinha");
		    messageProducer.send(message);
		} finally  {
			try {
			connection.close();
			} catch (Exception e){
				
			}
			
		}
		connectionFactory = JMSUtil.getConnectionFactory();
	}
	
	
	public static void main(String[] args) throws Exception{
		new Publisher().sendMessage();
	}
	

}
