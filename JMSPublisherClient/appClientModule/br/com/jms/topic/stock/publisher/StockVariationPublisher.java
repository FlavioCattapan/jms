package br.com.jms.topic.stock.publisher;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import br.com.jms.util.JMSUtil;
import br.com.jms.util.StockInfo;


public class StockVariationPublisher {

	private ConnectionFactory cf;
	private Connection connection;
	private Session session;
	// � um objeto administrado que encapsula a identidade do destino das
	// mensagens,
	// que � onde as mensagens s�o enviadas e consumidas. Pode ser uma fila ou
	// um t�pico.
	// O administrador JMS cria estes objetos, e os usu�rios os obt�m atrav�s de
	// buscas na JNDI.
	// Da mesma forma que as factories de conex�es,
	// o administrador pode criar dois tipos de classe de destino, fila e
	// t�pico.
	private Destination destination;
	// Um objeto criado atrav�s de uma sess�o JMS. Ele envia mensagens para um destino
	private MessageProducer messageProducer;
	private static long fileTs;

	public StockVariationPublisher() throws Exception {
		try {
			cf = JMSUtil.getConnectionFactory();
			destination = JMSUtil.getDestination();
			connection = cf.createConnection();
			// sess�o n�o ser� transacional
			// n�o precisa chamar o metodo commit
			// O recebimento da mensagens ser� notificado automaticamente.
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			messageProducer = session.createProducer(destination);
		} catch (Exception e) {
			try {
				connection.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			throw e;
		}

	}
    // m�todo respons�vel por obter os dados do arquivo
	private Map getFileData() throws Exception {
		File file = new File("./appClientModule/stock.txt");
		if (file.lastModified() == fileTs) {
			return null;
		}
		FileInputStream fis = new FileInputStream(file);
		try {
			Properties properties = new Properties();
			properties.load(fis);
			fileTs = file.lastModified();
			return properties;
		} finally {
			try {
				fis.close();
			} catch (Exception e) {

			}
		}
	}
   
	// m�todo respon�vel por transformar o map lido no arquivo em uma lista
	private List<StockInfo> toStockInfoList(Map data) {
		Set keys = data.keySet();
		List<StockInfo> list = new ArrayList<StockInfo>();
		for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			String val = (String) data.get(key);
			String[] tmp = val.split(",");
			StockInfo stockInfo = new StockInfo(key,
					Double.parseDouble(tmp[0]), Float.parseFloat(tmp[1]));
			list.add(stockInfo);
		}
		return list;
	}

	// metodo responsavel por criar e enviar a mensagem
	private void publish(List<StockInfo> list) throws Exception {
		// cria uma mensagem
		// cast de list para Serializable
		Message message = session.createObjectMessage((Serializable) list);
		messageProducer.send(message);
	}

	// metodo respons�vel por ler o arquivo de 1 em 1 segundo e enviando a mensagem
	private void scan() throws Exception {
		while (true) {
		    // obt�m os dados do arquivo
			Map fileData = getFileData();

			if (fileData != null) {
				// transforma a lista em map
				List<StockInfo> list = toStockInfoList(fileData);
				// envia a lista
				publish(list);
				System.out.println("New messagem published to topic");
			}

			Thread.sleep(1000);

		}

	}

	public static void main(String[] args) throws Exception {
		new StockVariationPublisher().scan();
	}

}
