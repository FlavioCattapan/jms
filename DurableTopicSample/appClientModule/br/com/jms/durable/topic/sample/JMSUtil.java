package br.com.jms.durable.topic.sample;

import javax.jms.Topic;
import javax.jms.TopicConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;

public class JMSUtil {

	private static final String CONNECTION_FACTORY = "TopicConnectionFactory";
	private static final String TOPIC = "topic/testDurableTopic";
	private static Context jndiContext = null;

	// InitialContext le o arquivo jndi.properties que especifica onde procurar
	// os objetos
	// java.naming.factory.initial = classe ''factory'' do contexto
	// java.naming.provider.url = URL definindo a localiza��o do objeto
	private static Object jndiLookup(String jndiName) throws Exception {
		if (jndiContext == null) {
			try {
				jndiContext = new InitialContext();
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		return jndiContext.lookup(jndiName);
	}

	public static TopicConnectionFactory getConnectionFactory()
			throws Exception {
		return (TopicConnectionFactory) jndiLookup(CONNECTION_FACTORY);
	}

//	As mensagens enviadas para t�picos JMS normais s� podem ser recebidas pelos assinantes
//	que estiverem conectados no momento do envio. Dessa forma, se um assinante estiver
//	desconectado ele perder� as mensagens enviadas durante o per�odo off-line.
//	A especifica��o JMS define um tipo especial de t�pico que armazena as mensagens 
//	dos assinantes desconectados e as envia assim que eles se conectarem. Esses s�o os t�picos dur�veis.
	public static Topic getDurableTopic() throws Exception {
		return (Topic) jndiLookup(TOPIC);
	}

}
