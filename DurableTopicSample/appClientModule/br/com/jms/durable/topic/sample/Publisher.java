package br.com.jms.durable.topic.sample;

import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;

public class Publisher {

	private TopicConnectionFactory cf;
	private TopicConnection connection;
	private TopicSession session;
	private Topic topic;
	private TopicPublisher publisher;

	public static void main(String[] args) throws Exception{
		Publisher publisher = new Publisher();
		publisher.sendMessage();

	}

	public Publisher() throws Exception {
		init();
	}

	private void init() throws Exception {
		try {
			cf = JMSUtil.getConnectionFactory();
			topic = JMSUtil.getDurableTopic();
			connection = cf.createTopicConnection();
			// O topico n�o � transacional n�o precisa dar commit ou rollback
			// O recebimento da mensagens ser� notificado automaticamente.
			session = connection.createTopicSession(false,
					Session.AUTO_ACKNOWLEDGE);
			publisher = session.createPublisher(topic);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	// m�todo respons�vel por criar e enviar a mensagem
	public void sendMessage() throws Exception {
		Message message = session.createTextMessage("Mensagem enviada");
	    publisher.publish(message);
		System.out.println("Message sucessfuly send");
	}

}
