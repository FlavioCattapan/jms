package br.com.jms.durable.topic.sample;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;

// classe respons�vel em receber as mensagens de forma ass�ncrona
public class Subscriber implements MessageListener {
	
	private TopicConnectionFactory cf;
	private TopicConnection connection;
	private TopicSession session;
	private Topic topic;
	private TopicSubscriber subscriber;
	
	// m�todo respons�vel por inicializar o recebimento de mensagens
	private void startListener() throws Exception {
		try {
			cf = JMSUtil.getConnectionFactory();
			topic = JMSUtil.getDurableTopic();
			connection = cf.createTopicConnection("dynsub","dynsub");
			connection.setClientID("CID");
			// O recebimento da mensagens ser� notificado automaticamente.
			session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			subscriber = session.createDurableSubscriber(topic, "MySubId");
			subscriber.setMessageListener(this);
			connection.start();
			
		} catch (Exception e) {
			 try {
				connection.close();
			} catch (Exception e2) {
				e.printStackTrace();
			}
			 throw e;
		}
		
	}
	
    // m�todo chamado sempre que uma mensagem � recebida  
	public void onMessage(Message message) {
		System.out.println(message);

	}

	public static void main(String[] args) throws Exception{
		Subscriber subscriber = new Subscriber();
		subscriber.startListener();
		//pausando a opera��o
		System.out.println("Listening for messages...");
		System.in.read();

	}

}
